package com.classpath.oauth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.oauth.model.User;

@RestController
@RequestMapping("/api/v1")
public class UserController {
	
	@GetMapping("/users")
	public User users() {
		return new User("Kiran", "USER");
	}

	@GetMapping("/admins")
	public User admins() {
		return new User("Vinay", "ADMIN");
	}

	@GetMapping("/managers")
	public User managers() {
		return new User("Hari", "MANAGER");
	}
}
